#!/usr/bin/env bash

# @file    install_cmake.sh
# @author  Hayat Rajani  [hayatrajani@gmail.com]

# set cmake version
CMAKE_VERSION=3.18.4

# download and build from source
wget https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/cmake-$CMAKE_VERSION.tar.gz
tar -zxvf cmake-$CMAKE_VERSION.tar.gz
cd cmake-$CMAKE_VERSION
./bootstrap
make
make install

# cleanup
cd .. && rm -rf cmake-$CMAKE_VERSION cmake-$CMAKE_VERSION.tar.gz
